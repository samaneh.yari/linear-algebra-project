# -*- coding: utf-8 -*-
"""
Created on Thu Jan 22 17:33:38 2019

@author: Samaneh Yari
"""
import random
import cv2
import os
from tqdm import tqdm
from os import walk
import numpy as np
import matplotlib.pyplot as plt
from scipy import linalg as la
from matplotlib.pyplot import imread

#Giving path for training images
ImagePath = 'D:\Projects\9631424_Samaneh_Yari\Train_Images'
ImageFile = os.listdir(ImagePath)
images = []

#Readling all the images and storing into an array
for name in ImageFile:
    temp = cv2.imread(ImagePath+name)
    temp = cv2.cvtColor(temp,cv2.COLOR_BGR2GRAY)
    temp = cv2.resize(temp, (100,100), interpolation = cv2.INTER_AREA)
    images.append(temp.flatten())
    
#Substractng mean from all images for normalization    
images = np.array(images)
mu = np.mean(images)
images = images-mu
images = images.T
print (images.shape)

#SVD function
u,s,v = np.linalg.svd(images, full_matrices=False)
print (u.shape, s.shape , v.shape)

#Reading test image as an input, converting into 100*100 
test = np.array(cv2.imread('D:\Projects\9631424_Samaneh_Yari\Test_Images'))
test = cv2.cvtColor(test,cv2.COLOR_BGR2GRAY)
test = cv2.resize(test, (100,100), interpolation = cv2.INTER_AREA)

img = test.reshape(1, -1)

#Substracting mean
img = img-mu

img = img.T
print (img[:][50])

#Dot product of test image and U matrix
test_x = np.empty(shape = (u.shape[0], u.shape[1]),  dtype=np.int8)
print (test_x.shape)

for col in range(u.shape[1]):    
    test_x[:,col] = img[:,0] * u[:,col]
dot_test = np.array(test_x, dtype='int8').flatten()
#Divide train and test images
def plot(img, w=100, h=100):
    plt.imshow(img.reshape((w,h)), cmap='gray', interpolation='nearest')
    plt.show()
def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])
def readImage(path):
    return rgb2gray(imread(path)).ravel()
def read_data(path):
    trainSet = []
    trainLabels = []
    testSet = []
    testLabels = []
    for (dirpath, dirnames, filenames) in walk(path):
        if filenames:
            lable_name = dirpath[dirpath.rfind("/")+1:]
            files_shuffle = [os.path.join(dirpath, filename) for filename in filenames if filename.endswith("jpg")]
            random.shuffle(files_shuffle)
            train_img = files_shuffle[0]
            test_imgs = files_shuffle[1:]
            trainSet.append(readImage(train_img))
            trainLabels.append(lable_name)
            testSet = testSet + [readImage(img) for img in test_imgs]
            testLabels = testLabels + [lable_name for _ in test_imgs]
    return np.transpose(trainSet), trainLabels, np.transpose(testSet), testLabels
trainSet, trainLabels, testSet , testLabels = read_data(ImagePath)
print(trainSet.shape, testSet.shape)

#Dot product of all the images and U matrix
dot_train = np.empty(shape = (u.shape[0]*u.shape[1], u.shape[1]),  dtype=np.int8)
temp = np.empty(shape = (u.shape[0], u.shape[1]),  dtype=np.int8)


for i in range(images.shape[1]):
    
    for c in range(u.shape[1]):    
        temp[:,c] = images[:,i] * u[:,c]
        
    tempF = np.array(temp, dtype='int8').flatten()
    dot_train[:, i] = tempF[:]

#Substracting Two dot products
sub = np.empty(shape = (u.shape[0]*u.shape[1], u.shape[1]))
for col in range(u.shape[1]):
    sub[:,col] = dot_train[:,col] - dot_test[:]

#Finding norm of all the colums
answer = np.empty(shape=(u.shape[1],))

for c in range(sub.shape[1]):    
    answer[c] = np.linalg.norm(sub[:,c])
print (answer)

#Sorting answer array and retriving first element which will be minimum from all
temp_ans = np.empty(shape=(u.shape[1],))
temp=np.copy(answer)
temp.sort()
check = temp[0]
print (check)
index=0
for i in range(answer.shape[0]):
    if check == answer[i]:
        index=i
        break
    
#example grayscale face image from db
plot(trainSet[:, 0])

# Image from train dataset whithout mean image 
trainSetWihoutMean =  trainSet - np.vstack(img)

#example image
plot(trainSetWihoutMean[10:10, 0])
U, s, Vh = la.svd(trainSetWihoutMean, full_matrices=False)
csum = np.cumsum(s)

#distribution of singulr values from biggest to smallest
plt.plot(csum/csum[-1])

#number of principal components , we define K=25 based on required
k=25
new_base = u[:,:k-1].T

#projectin of taining images set on space with base=new_base
trainSetProjection = np.dot(new_base, trainSetWihoutMean)

#Checking for corresponding image for minimum answer
folder_tr = 'D:\Projects\9631424_Samaneh_Yari\Train_Images'
i = 0
print (index)
for filename in os.listdir(os.getcwd()+"/"+folder_tr):
    if index == i:
        print ("The predicted face is: ",filename)
        break
    else:
        i=i+1
test_set_size = 100

#sample 100 images from test set 
random_sub_sample = random.sample(range(len(testLabels)), test_set_size)
correct_guesses = 0
for i in tqdm(random_sub_sample):
    test_img = np.array([testSet[:,i]]).T
    correct_label = testLabels[i]

    test_img_diff = test_img - np.vstack(img)
    
#Projection of input image on lower dimension
    test_img_projection = np.dot(new_base, test_img_diff)
    guess_index = np.argmin(np.linalg.norm(trainSetProjection - test_img_projection, axis=0))
    guess_label = trainLabels[guess_index]
    
    if correct_label == guess_label:
        correct_guesses += 1

accuracy = 1.0*correct_guesses/test_set_size
print("accuracy : ", accuracy*100)